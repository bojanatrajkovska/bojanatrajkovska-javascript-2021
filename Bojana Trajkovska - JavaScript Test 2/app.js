$(function () {
	let id=1;
    const app = new App();
    function App(){
        this.books = [];
        this.shoppingCard = [];
        this.booksToWishList = [];

        this.addBook = function(book){
            this.books.push(book);
            this.printBooks();
        }

        this.printBooks = function(){
            let htmlToAdd = "";
            const actionsBtn = `<button class="buy-btn btn buy"> buy </button>
                                <button class="wishlist-btn btn"> buy </button>`;
            for (let item of this.books){
                htmlToAdd += `<li id="${item.id}" class="card">
                    <div class="card-title"> ${item.title} </div>
                    <div class="card-img"> <img src="${item.cover}"/> </div>
                    <div class="card-description"> ${item.description} </div>
                    <div class="card-price"> Price: ${item.price} den.</div>
                    <div class="card-quantity"> Quantity: ${item.quantity > 0 ? item.quantity : 'out of stoke'} </div>
                    <div class="card-actions"> ${item.quantity > 0 ? actionsBtn : ''} </div>
                </li>`;
            }
            $("#books-list").html(htmlToAdd);
        }

        this.buyBook = function (bookId){
            const book = this.books.find(x => x.id === bookId);
            const bookIndex = this.books.indexOf(book);
            if (bookIndex != -1 && book.quantity > 0){
                this.shoppingCard.push(book);
                book.changeQuantity();
                this.printBooks();
                this.printShoppingCard();
            }
        }


        this.deleteBook = function(studentId) {
            const book = this.books.find(x => x.id === bookId); 
            if(book) {
                const index = this.books.indexOf(book);
                this.books.splice(index, 1);
                this.printBooks();
            }
        }

        this.clear = function() {
            $(".card-title").val("");
            $(".card-cover").val("");
            $(".card-description").val("");
            $(".card-price").val("");
            $(".card-quantity").val("");
        }

        

        this.listProductsInShoppingCard = function() {
            const element = document.querySelector("#shopping-card");
            let htmlToAdd = "";
            for(let item of this.shoppingCartProducts) {
                htmlToAdd += `<li> 
                    ${item.name} - ${item.price}    
                </li>`;
            }
    
            element.innerHTML = htmlToAdd;
        }



        this.addToCard = function(product) {
            this.shoppingCartProducts.push(product);
            this.listProductsInShoppingCard();
        }





    }


    function Book(title,  cover, price, quantity, description) {
        
        this.title = title;
        this.cover = cover;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.id = id++;
    }


    document.addEventListener("click", function(e) {
        if(e.target.classList.contains("buy-btn")) {
            const productIndex = parseInt(e.target.closest("li").getAttribute("id"));
            const product = app.getProductByIndex(productIndex);
            if(product) {
                product.quantity--;
                document.getElementById("quantities-" + productIndex).innerHTML = "Quantity: " + product.quantity;
                app.addToCart(product);
            }
        }
    });

    document.addEventListener("click", function(e) {
        if(e.target.classList.contains("wishlist-btn")) {
            const productIndex = parseInt(e.target.closest("li").getAttribute("id"));
            const product = app.getProductByIndex(productIndex);
            if(product) {
                product.quantity--;
                document.getElementById("quantities-" + productIndex).innerHTML = "Quantity: " + product.quantity;
                app.addToCart(product);
            }
        }
    });

    $("#add-btn").on("click", function(e){
        e.preventDefault();
        book = new Book($("#title").val(),$("#cover").val(),$("#price").val(),$("#quantity").val(),$("#description").val(),$("#quantity").val())
        app.addBook(book);


    })

    $(document).on("click", "remove-book-card", function() {
        console.log('delete button was pressed', $(this).parents("li"));
        const index =  $(this).parents("li").attr("id");
        app.deleteBook(parseInt(index));
    });

    $(document).on("click", "remove-book-wish", function() {
        console.log('delete button was pressed', $(this).parents("li"));
        const index =  $(this).parents("li").attr("id");
        app.deleteBook(parseInt(index));
    });



    $(document).on("click", "remove-book-card", function(){
        e.preventDefault();
        app.clear();
    });

    $(document).on("click", "remove-book-wish", function(){
        e.preventDefault();
        app.clear();
    });


    $(document).on("click", ".buy-btn", function(){
        const bookId = parseInt($(this).parents("li").attr("id"));
        app.addToCard(bookId);
    });

    $(document).on("click", ".wishlist-btn", function(){
        const bookId = parseInt($(this).parents("li").attr("id"));
        app.addToCart(bookId);
    });

    $(document).on("click", ".remove-book-card", function(){
        const bookId = parseInt($(this).attr("id"));
        app.removeBookFromShoppingCard(bookId);
    });

    $(document).on("click", ".remove-book-wish", function(){
        const bookId = parseInt($(this).attr("id"));
        app.removeBookFromWishList(bookId);
    });

});