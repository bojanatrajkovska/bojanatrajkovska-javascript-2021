//Exercise 1
function Me(name, year){
    let yearsNow = 2021 - year; 
    console.log(`Hi ${name} you're ${yearsNow} old`);
}

Me("John", 1989);
Me("Bojana", 1998);
Me("Mina", 1996);
Me("Lina", 1995);

//Exercise 2
let proizvod = prompt("Vnesi ovosje");
switch (proizvod) {
    case 'Kivi':
        console.log(150);
        break;
    case 'Portokali':
        console.log(200);
        break;
    case 'Banani':
        console.log(250);
        break;
    case 'Jabolka':
        console.log(100);
        break;
    case 'Krusi':
        console.log(550);
        break;
    case 'Creshi':
        console.log(1250);
        break;
    case 'Mandarini':
        console.log(120);
        break;
    case 'Citron':
        console.log(50);
        break;
    case 'Avokado':
        console.log(130);
        break;
    case 'Pomelo':
        console.log(300);
        break;
    case 'Ananas':
        console.log(450);
        break;
    default:
        console.log('Proizvodot go nema vo listata');
        break;
}

//Exercise 2
function getPrice(item) {
    //debugger;
    switch (item) {
        case "kiwi":
            return 150;
        case "portokali":
            return 250;
        case "banani":
            return 180;
        case "krusi":
            return 550;
        case "pomelo":
            return 350;
        default:
            return `pobaraniot proizvod: ${item} go nema vo listata`;
    }
}

    //Part 2
    function calculatePrice(itemPrice, quantity){
        return itemPrice * quantity;
    }

    //const kiwiPrice = getPrice("kiwi");
    //console.log(kiwiPrice);
    //const krusiPrice = getPrice("krusi");
    //console.log(krusiPrice);

    let itemName = prompt("vnesi ovosje");
    const itemPrice = getPrice(itemName);

    let quantity = parseInt(prompt("vnesi kolicina vo kg"));
    const result = calculatePrice(itemPrice, quantity);
    console.log(`${quantity} kilogram(s) ${itemName} kosta ${result} denari`);

    //Drug nacin 
    const kiwiPrice = getPrice("kiwi");
    calculatePrice(kiwiPrice, 5);




    //Exercise 2
    let cena = 0;
    let proizvodVid = prompt("Vnesi proizvod");
    let kolicina = prompt ("Vnesi kolicina");

    switch(proizvodVid){
        case "kivi":
            cena = 150 * kolicina;
            console.log(`${kolicina} kilogram(i) kosta ${cena} denari`);
            break;
        case "portokali":
            cena = 200 * kolicina;
            console.log(`${kolicina} kilogram(i) kosta ${cena} denari`);
            break;
        case "banani":
            cena = 250 * kolicina;
            console.log(`${kolicina} kilogram(i) kosta ${cena} denari`);
            break;
        case "jabolka":
            cena = 100 * kolicina;
            console.log(`${kolicina} kilogram(i) kosta ${cena} denari`);
            break;
        case "krusi":
            cena = 550 * kolicina;
            console.log(`${kolicina} kilogram(i) kosta ${cena} denari`);
            break;
        default:
            console.log("Go nema proizvodot");
    }

    //Exercise 3 
    // najdi element sto se naogja do drug element vo niza

function najdiSledenElement(arr, num1){
    for(let i = 0; i < arr.length; i++){
        if(num1 === arr[i]){
            return(arr[i+1]);
        }else{
            return -1;
        }
    }
}
console.log(najdiSledenElement([2,5,44,22,1143,45,22,0], 0));
