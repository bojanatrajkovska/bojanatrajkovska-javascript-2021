//Exercise 1
const articles = ["pc", 1000, "mouse", 200, "keyboard", 500, "headphones", 400, "mic", 200];

function addArticle(name, price){
    articles.push(name, price);
    //console.log(articles);
}

addArticle("mouse pad", 350);
addArticle("speakers", 1350);

function getArticle(name){
    for (let i = 0; i < articles.length; i++) {
        if(articles[i] === name){
            console.log(`${name} is on ${i} index/position`);
            return i;
        }
    }
    return -1;
}

console.log(getArticle("keyboard"));

function printArticles(){   // article i , i+1   // log: reden broj, ime na article, cena na article
    let counter = 1;
    for (let i = 0; i < articles.length; i++) {
        console.log(`${counter}: ${articles[i]} - ${articles[i+=1]}`);
        counter++;
    }
}

printArticles();

function editPrice(name, newPrice){
    //1 find article[i]  <- name
    //2 edit article[i+=1]  <- price    article[i+=1] = newPrice
    let index = getArticle(name);
    if(index > -1){
        articles[index+=1] = newPrice;
        console.log(`The new price for ${name} is ${articles[index]}`);
    }
 // for (let i = 0; i < articles.length; i++) {
    //    if(articles[i] === name){
    //        articles[i+=1] = newPrice;
    //        console.log(`The new price for ${name} is ${articles[i]}`);
    //    }
    //  }
}

editPrice("mouse", 100); //log: The new price for mouse is 100
printArticles();

function deleteArticle(name){
    //1 find article[i]  <- name
    //splice(i,2)

    let index = getArticle(name);
    if(index > -1){
        articles.splice(i, 2);
        console.log(`The ${name} was deleted`);
    }

 //   for (let i = 0; i < articles.length; i++) {
  //       if(articles[i] === name){
  //           articles.slice(i,2);
  //           console.log(`The ${name} was deleted`);
  //       }
 //    }
 //}
    articles.splice(1, 2, "pc"); 
    console.log(`${articles} was deleted`);


deleteArticle("pc"); //log: Pc was deleted
printArticles();

function getPrice(name){
    //1 find article[i]  <- name
    //2 edit article[i+=1]  <- price 

    let index = getArticle(name);
    if(index > -1){
        console.log(`The article: ${articles[index]} has price of ${articles[index+1]}`);
    }
 //   for (let i = 0; i < articles.length; i++) {
 //       if(articles[i] === name){
 //           articles.slice(i,2);
 //           console.log(`The article: ${articles[i]} has price of ${articles[i+1]}`);
 //       }
 //   }
//}

getPrice("mouse"); //log: The price for mouse is 200
getPrice("mouse pad"); //log: The price for mouse pad is 350

