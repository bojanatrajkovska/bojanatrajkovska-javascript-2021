
// Task 4
// 4.1
const car = {
    model: "Lada Samara",
    color: "white",
    year: 2010,
    fuel: "benzin",
    fuelConsumation: 6.6,

    calculateFuelForDistance: function (distance) {
        return `${(this.fuelConsumation * distance) / 100}l ${this.fuel}`;
    },

    printInfo: function () {
        return `The car ${this.model} is ${new Date().getFullYear() - this.year} years old and has ${this.color} color`;
    }
};

console.log(car.calculateFuelForDistance(250));
console.log(car.printInfo());
// The car is _______ years old and has a ________ color

// 4.2
function Car(model, color, year, fuel, fuelConsumation) {
    this.model = model;
    this.color = color;
    this.year = year;
    this.fuel = fuel;
    this.fuelConsumation = fuelConsumation;

    this.calculateFuelForDistance = function (distance) {
        return `${(this.fuelConsumation * distance) / 100}l ${this.fuel}`;
    };

    this.printInfo = function () {
        return `The car ${this.model} is ${new Date().getFullYear() - this.year} years old and has ${this.color} color`;
    };
};

// instances
var bmw = new Car("bmw", "black", "2016", "dizel", 10);
console.log(bmw.printInfo());
console.log(bmw.calculateFuelForDistance(100));

var yugo = new Car("yugo", "white", "2000", "plin", 6.8);
console.log(yugo.printInfo());
console.log(yugo.calculateFuelForDistance(260));

// 4.3
// Car with HTML
const model = document.querySelector("#model");
const color = document.querySelector("#color");
const year = document.querySelector("#year");
const fuel = document.querySelector("#fuel");
const fuelConsumation = document.querySelector("#fuel-consumation");
const distance = document.querySelector("#distance");
const createBtn = document.querySelector("#create-btn");
const calculateBtn = document.querySelector("#calculate-btn");
const printDetails = document.querySelector("#p-details");
const printCalculations = document.querySelector("#p-calculations");
const carList = document.querySelector("#car-list");

const cars = [];
createBtn.addEventListener("click", function (e) {
    e.preventDefault();

    const newCar = new Car(model.value, color.value, year.value, fuel.value, fuelConsumation.value);
    cars.push(newCar);
    printCarListItem(newCar, cars.length-1);

    //calculateBtn.disabled = false;
    //printDetails.innerText = car123.printInfo();
});

function printCarListItem(carToPrint, index) {
    carList.innerHTML += `<li> 
        ${carToPrint.model} ${carToPrint.year} 
        <button class="detail-btn" id="item-${index}"> details </button> 
        <button> calculate </button> 
        <div> print detail and calculations </div> 
    </li>`;
};

document.addEventListener("click", function(e) {
    if(e.target.classList.contains("detail-btn")) {
        const index = Number(e.target.id.split("-")[1]);
        console.log(cars[index]);
        console.log(cars[index].printInfo());
        
    }
});

// calculateBtn.addEventListener("click", function(e) {
//     e.preventDefault();
//     printCalculations.innerText = car123.calculateFuelForDistance(distance.value);;
// });