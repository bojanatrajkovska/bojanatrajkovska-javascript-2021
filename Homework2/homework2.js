//Exercise 1
//change element

let firstDiv = document.getElementById("myTitle");
firstDiv.innerText += " Nov dopisan tekst na h1 so id myTitle";

let firstParagraph = document.getElementsByClassName("paragraph")[0];
firstParagraph.innerText += " Nov dopisan tekst na prv paragraph";

let secondParagraph = document.getElementsByClassName("paragraph")[1];
secondParagraph.innerText += " Nov dopisan tekst na vtor paragraph";

//let text = document.querySelector("text");
//text.innerText  += " Nov dopisan tekst na text";

let text = document.getElementsByTagName("div")[1].lastElementChild;
text.innerText += "  Nov dopisan tekst na h1";

let h1Last = document.getElementsByTagName("div")[2].firstElementChild;
h1Last.innerText += "  Nov dopisan tekst na h1";

//let h3 = document.querySelector("h3");
//h3.innerText  += " Nov dopisan tekst na h3";

let h3Last = document.getElementsByTagName("div")[2].lastElementChild;
h3Last.innerText += "  Nov dopisan tekst na h3";