function Store(name) {
    this.name = name;
    this.products = [];
    this.shoppingCartProducts = [];

    this.addProduct = function(product) {
        this.products.push(product);
    }

    this.listProducts = function() {
        const element = document.querySelector("#products");
        let htmlToAdd = "";
        let index = 0;
        for(let item of this.products) {
            htmlToAdd += `<li data-index="${index}"> 
                <h4> Name: ${item.name} </h4>
                <div> <img src="${item.imageUrl}" width="150" alt="${item.name}"/> </div> 
                <div> Price: ${item.price} denari </div> 
                <div id = "quantities-${index}"> Quantity: ${item.quantity} </div>
                <div> Description: ${item.description} </div>
                <div> Compare: <input type="checkbox" class="compare-chk" /> </div>
                <div> <button class="add-to-cart-btn"> add to cart </button> </div>
            </li>`;
            index++;
        }

        element.innerHTML = htmlToAdd;
    }

    this.listProductsInShoppingCart = function() {
        const element = document.querySelector("#shopping-cart");
        let htmlToAdd = "";
        for(let item of this.shoppingCartProducts) {
            htmlToAdd += `<li> 
                ${item.name} - ${item.price}    
            </li>`;
        }

        element.innerHTML = htmlToAdd;
    }

    this.getProductByIndex = function(index) {
        let product = this.products[index];
        if(product) {
            return product;
        } else {
            return false;
        }
    }

    this.addToCart = function(product) {
        this.shoppingCartProducts.push(product);
        this.listProductsInShoppingCart();
    }

    this.compareProducts = function(product1, product2) {
        const element = document.querySelector("#compare");
        element.innerHTML = `<table border="1">
                <tr>
                    <th> ${product1.name} </th>
                    <th> ${product2.name} </th>
                </tr>
                <tr>
                    <td> <img src="${product1.imageUrl}" width="60" /> </td>
                    <td> <img src="${product2.imageUrl}" width="60" /> </td>
                </tr>
                <tr>
                    <td class="${ product1.price < product2.price ? 'cheaper-product' : '' }"> ${product1.price} denari </td>
                    <td class="${ product2.price < product1.price ? 'cheaper-product' : '' }"> ${product2.price} denari </td>
                </tr>
                <tr>
                    <td> ${product1.description} </td>
                    <td> ${product2.description} </td>
                </tr>
            </table>`;
    }
}

function Product(name, price, imageUrl, description, quantity) {
    this.name = name;
    this.price = price;
    this.imageUrl = imageUrl;
    this.description = description;
    this.quantity = quantity;
}

const tvSamsung = new Product("Samsung X5021", 26500, "https://bit.ly/3qQC6aM", "Full HD Ready, 65' usbx3", 6);
const tvLg = new Product("LG L100x", 50000, "https://bit.ly/38NWyCR", "4K Screen, extra quality sound", 10);
const tvPhilips = new Product("Philips ph006", 16500, "https://bit.ly/3lmzeky", "UHD with led backlight", 20);
const tvSony = new Product("Sony sn001", 68000, "https://bit.ly/3qXwhrP", "4K extreme image and sound quality", 2);

const myStore = new Store("SEDC store");
myStore.addProduct(tvSamsung);
myStore.addProduct(tvLg);
myStore.addProduct(tvPhilips);
myStore.addProduct(tvSony);

myStore.listProducts();

document.addEventListener("click", function(e) {
    if(e.target.classList.contains("add-to-cart-btn")) {
        const productIndex = parseInt(e.target.closest("li").getAttribute("data-index"));
        const product = myStore.getProductByIndex(productIndex);
        if(product) {
            product.quantity--;
            document.getElementById("quantities-" + productIndex).innerHTML = "Quantity: " + product.quantity;
            myStore.addToCart(product);
        }
    }
});

let productsToCompare = [];
document.addEventListener("input", function(e) {
    if(e.target.classList.contains("compare-chk")) {
        const productIndex = parseInt(e.target.closest("li").getAttribute("data-index"));
        const product = myStore.getProductByIndex(productIndex);

        if(productsToCompare.length < 3) {
            productsToCompare.push(product);
        }

        if(productsToCompare.length === 2) {
            myStore.compareProducts(productsToCompare[0], productsToCompare[1]);
            productsToCompare = [];
            uncheckCompareInputs();
        }
    }
});

function uncheckCompareInputs() {
    const inputs = document.querySelectorAll(".compare-chk");
    for(let item of inputs) {
        item.checked = false;
    }
}


