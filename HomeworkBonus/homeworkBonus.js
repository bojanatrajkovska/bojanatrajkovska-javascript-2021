//Exercise 1
let header = document.getElementById("myTitle");
let listDiv = document.getElementById("list");

let name = prompt("Name of recipe?");
let number = prompt("Number of ingredients?");

let nameIngredient = [];
for(let i=0; i<number; i++){
    nameIngredient.push(prompt("Names of ingredients?"));
}

header.innerText = `${name}`;

let htmlAsString = "";
htmlAsString += "<ul>";
for(let i=0; i<nameIngredient.length; i++){
    htmlAsString += `<li> ${nameIngredient[i]}</li>`;
}
htmlAsString += "</ul>";

listDiv.innerHTML += htmlAsString;