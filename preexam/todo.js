// Task 1
const users = ["admin@sedc.com", "student@sedc.com", "user@sedc.com", "tester@sedc.com"];
const passwords = ["admin123", "student123", "user123", "tester123"];

const loginBtn = document.getElementById('login-btn');
const addTaskBtn = document.getElementById('add-task-btn');
const usernameElement =  document.getElementById('username');
const passwordElement =  document.getElementById('password');
const loginDiv = document.getElementById('login-status');

loginDiv.innerText = '';

loginBtn.addEventListener('click', usernameAndPassword);

function usernameAndPassword(){
    let username = usernameElement.value;
    let password = passwordElement.value;
    
    if(username){
        let index = users.indexOf(username);

        if(index >= 0){

            if(password === passwords[index]){
                loginDiv.innerText = 'Welcome ' + username;
            }
            else{
                loginDiv.innerText =  'Wrong Credentials';
            }

        }
        else{
            loginDiv.innerText =  'Wrong Credentials';
        }
    }
    else{
        loginDiv.innerText =  'Wrong Credentials';
    }
    

};


//Task 2

const toDoList = document.getElementById('todo-list');
const total = document.getElementById('total');
const done = document.getElementById('done');
const left = document.getElementById('left');

addTaskBtn.addEventListener('click', addTaskInput);

let totalInSpan = 0;
let doneInSpan = 0;

function addTaskInput(){

    let addTask = document.getElementById('add-task').value;

    if(addTask){
        let li = document.createElement('li');

        let spanInList = document.createElement('span');
        spanInList.innerText = addTask;
        li.append(spanInList);
        
        let checkbox = document.createElement('input');
        checkbox.setAttribute('type', 'checkbox');
        checkbox.addEventListener('click', function(){
            if(checkbox.checked){
                doneInSpan++
                spanInList.classList.add('task-done');
            }
            else{
                doneInSpan--
                spanInList.classList.remove('task-done');
            }
            totalDoneAndLeft();
        });

        li.append(checkbox);

        toDoList.append(li);
        document.getElementById('add-task').value = '';
        totalInSpan++;
        totalDoneAndLeft();
    }
}

function totalDoneAndLeft(){
    total.innerText = totalInSpan;
    done.innerText = doneInSpan;
    left.innerText = totalInSpan - doneInSpan;
}