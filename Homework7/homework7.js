// 1. Literal notation
const hotelRadika = {
    name: "Radika", // property, key:name, "Radika": value
    rooms: 40,
    booked: 20,
    gym: true,
    roomType: ["twin", "double", "single"],

    checkAvailability: function () { // method
        return this.rooms - this.booked;
    }
};

// access the object's properties or methods
console.log(hotelRadika.name);
console.log(hotelRadika.rooms); // dot notation
console.log(hotelRadika["rooms"]); // square
console.log(hotelRadika.checkAvailability());

// card example
function printToHTML(selectorName, objectName) {
    document.querySelector(`${selectorName} img`).src = objectName.imageUrl;
    document.querySelector(`${selectorName} h3`).innerText = objectName.title;
    document.querySelector(`${selectorName} p`).innerText = objectName.shortContent;
    document.querySelector(`${selectorName} button`).innerText = objectName.buttonContent;
}

const card1 = {
    imageUrl: "https://via.placeholder.com/350",
    title: "Policki chas od 22 do 05h",
    shortContent: "Shok shok poglednete shot mu kaza Pece na Mende (video + audio)",
    buttonContent: "procitajte poveke",
    tags: ["policija", "covid19", "sediDoma", "uciJS"]
};
//printToHTML(".card1", card1);

const card2 = {
    imageUrl: "https://via.placeholder.com/350",
    title: "SEDC Studentite pocnaa da izucuvaat JS OOP",
    shortContent: "OOP so literal notation zvuci ednostavno za korist...",
    buttonContent: "procitajte poveke"
};
//printToHTML(".card2", card2);

const card3 = {
    imageUrl: "https://via.placeholder.com/350",
    title: "News number 3",
    shortContent: "Lorem ipsum dolorem...",
    buttonContent: "read more"
};
//printToHTML(".card3", card3);

// probajte sami :) <article> od HTML da se generira/crta od JS, kolku card objekti postojat tolku da se dodadat artikli

// use-case
const appInfo = {
    clientName: "SEDC",
    dateOfCreation: 2021,
    team: ["John", "Doe"],
    version: "1.0.0"
};

const shape = {
    width: 600
};
console.log("shape: ", shape);
shape["height"] = 400;
console.log("shape:", shape);
console.log("shape height:", shape.height);

// update objects
var hotel = {
    name: "Hotel 5",
    rooms: 45,
    booked: 10
};
const hotelName = hotel.name;
console.log("initial hotel name: ", hotelName);
hotel.name = "Hotel Star 5*";
console.log("new hotel name: ", hotel.name);
hotel.gym = false;
console.log("hotel", hotel);
delete hotel.gym;
console.log("hotel", hotel);


// 2. Constructor notation
const hotelR = new Object();
hotelR.name = "Radika 2";
hotelR.rooms = 40;
hotelR.booked = 15;
hotelR.checkAvailability = function () {
    return this.rooms - this.booked;
}

console.log(hotelR.name);
console.log(hotelR.rooms);
console.log(hotelR.checkAvailability());

// 3. Object with function constructor
function Hotel(name, rooms, booked, gym, roomType) {
    this.name = name;
    this.rooms = rooms;
    this.booked = booked;
    this.gym = gym;
    this.roomType = roomType;

    console.log("this", this);

    this.checkAvailability = function () {
        return this.rooms - this.booked;
    }
};

// instances
const hotelRadika2 = new Hotel("Radika", 40, 20, true, ["twins", "duoble"]);
hotelRadika2.checkAvailability(); 20

const hotel5 = new Hotel("Hotel 5", 100, 90, false, ["single"]);
hotel5.checkAvailability(); // 10

function doNothing() {
    console.log("this in regular function", this);
}
doNothing();

// task 1
// apply for a job form
// 1. select inputs
const name = document.querySelector("#name");
const lastname = document.querySelector("#lastname");
const education = document.querySelector("#education");
const age = document.querySelector("#age");
const sendBtn = document.querySelector("#send-btn");
// 2. attach click event
// 2.1 collect the values from inputs
let persons = [];
sendBtn.addEventListener("click", function (e) {
    e.preventDefault();
    const person = new Person(name.value, lastname.value, education.value, age.value);
    persons.push(person);
    console.log("persons:", persons);
});

// 3. create Person object
function Person(name, lastname, education, age) {
    this.name = name;
    this.lastname = lastname;
    this.education = education;
    this.age = age;
};

// Task 1
const student = {
    name: "David Rayy",
    class: "VI",
    rollno: 12,

    printInfo: function () {
        return `"${this.name}" "${this.class}", ${this.rollno}`;
    }
};

const teacher = {
    name: "prof.David Rayy",
    class: ["V1", "VII", "V"],

    printInfo: function () {
        return `"${this.name}" "${this.class}", ${this.rollno}`;
    }
};

// student.printData() => "David Rayy","VI", 12
//console.log(student.printInfo());

function getStudentInfo(student, teacher) {
    console.log(`"${student.name}" "${student.class}", ${student.rollno}`);
    console.log(`"${teacher.name}" "${teacher.class}"`);
}

//getStudentInfo(student, teacher);

// Task 2
//console.log(student.printInfo());
delete student.rollno;
console.log(student.printInfo());

// Task 3
function isPropertyExistInObject(object, property) {
    return object[property] !== undefined ? "postoi" : "ne postoi";
}

console.log(isPropertyExistInObject(student, "class")); // true student["class"] => "VI"
console.log(isPropertyExistInObject(student, "year"));  // false

// es6
console.log("es6: ", student.hasOwnProperty("year"));
console.log("es6 in: ", "class" in student);

// Task 4
// 4.1
const car = {
    model: "Lada Samara",
    color: "white",
    year: 2010,
    fuel: "benzin",
    fuelConsumation: 6.6,

    calculateFuelForDistance: function (distance) {
        return `${(this.fuelConsumation * distance) / 100}l ${this.fuel}`;
    },

    printInfo: function () {
        return `The car ${this.model} is ${new Date().getFullYear() - this.year} years old and has ${this.color} color`;
    }
};

console.log(car.calculateFuelForDistance(250));
console.log(car.printInfo());
// The car is _______ years old and has a ________ color

// 4.2
function Car(model, color, year, fuel, fuelConsumation) {
    this.model = model;
    this.color = color;
    this.year = year;
    this.fuel = fuel;
    this.fuelConsumation = fuelConsumation;

    this.calculateFuelForDistance = function (distance) {
        return `${(this.fuelConsumation * distance) / 100}l ${this.fuel}`;
    };

    this.printInfo = function () {
        return `The car ${this.model} is ${new Date().getFullYear() - this.year} years old and has ${this.color} color`;
    };
};

// instances
var bmw = new Car("bmw", "black", "2016", "dizel", 10);
console.log(bmw.printInfo());
console.log(bmw.calculateFuelForDistance(100));

var yugo = new Car("yugo", "white", "2000", "plin", 6.8);
console.log(yugo.printInfo());
console.log(yugo.calculateFuelForDistance(260));

// 4.3
// Car with HTML
const model = document.querySelector("#model");
const color = document.querySelector("#color");
const year = document.querySelector("#year");
const fuel = document.querySelector("#fuel");
const fuelConsumation = document.querySelector("#fuel-consumation");
const distance = document.querySelector("#distance");
const createBtn = document.querySelector("#create-btn");
const calculateBtn = document.querySelector("#calculate-btn");
const printDetails = document.querySelector("#p-details");
const printCalculations = document.querySelector("#p-calculations");
const carList = document.querySelector("#car-list");

const cars = [];
createBtn.addEventListener("click", function (e) {
    e.preventDefault();

    const newCar = new Car(model.value, color.value, year.value, fuel.value, fuelConsumation.value);
    cars.push(newCar);
    printCarListItem(newCar, cars.length-1);

    //calculateBtn.disabled = false;
    //printDetails.innerText = car123.printInfo();
});

function printCarListItem(carToPrint, index) {
    carList.innerHTML += `<li> 
        ${carToPrint.model} ${carToPrint.year} 
        <button class="detail-btn" id="item-${index}"> details </button> 
        <button> calculate </button> 
        <div class = "print" id = "printDetail-${index}"> ${carToPrint.printInfo()} </div> 
    </li>`;
};

document.addEventListener("click", function(e) {
    if(e.target.classList.contains("detail-btn")) {
        const index = Number(e.target.id.split("-")[1]);
        console.log(cars[index]);
        console.log(cars[index].printInfo());
        
    }
});

// calculateBtn.addEventListener("click", function(e) {
//     e.preventDefault();
//     printCalculations.innerText = car123.calculateFuelForDistance(distance.value);;
// });