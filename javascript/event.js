// 1 way
// select an element
const clickBnt = document.getElementById("click-me-btn");

function sayHello() {
    alert("Hello!");
}

// 2nd way
const click2Btn = document.getElementById("click-2");
// click2Btn.onclick = function() {
//     alert("You just clicked button 2");
// }
click2Btn.onclick = sayHello;

// 3th way
const click3Btn = document.getElementById("click-3");
click3Btn.addEventListener("click", sayHello, false);

// blur event example
const txt1 = document.getElementById("txt-1");
const txt1Result = document.getElementById("txt-1-result");

txt1.addEventListener("blur", function(e) {
    console.log(e);
    // e.target.value;
    txt1Result.innerText = `Zdravo ${e.target.value}`;
    txt1Result.innerText += `Target element has id: ${e.target.id}`;
    // txt1Result.innerText = `Zdravo ${txt1.value}`;
});

// box-1
const box1 = document.getElementById("box-1");
box1.addEventListener("mouseenter", function(){
    box1.style.backgroundColor = "orange";
});

box1.addEventListener("mouseleave", function() {
    box1.style.backgroundColor = "limegreen";
});

// username check
// 1 select element
const username = document.getElementById("username");
const usernameResult = document.getElementById("username-result");

// 2 implement the logic
// default param value => minLength = 3
function checkUsername(username, minLength = 3) {
    let result = "";
    if(username.length >= minLength) {
        //usernameResult.style.color = "green";
        result = `<span class="valid"> ${username} is valid </span>`;
    } else {
        //usernameResult.style.color = "red";
        result = `<span class="not-valid"> ${username} is not valid, the min chars is ${minLength} </span>`;
    }

    return result;
}

// 3 test the logic
// checkUsername("ad", 3); // not valid
// checkUsername("john", 3); // valid

// 4 event and event handling
username.addEventListener("blur", function(e){
    e.stopPropagation();
    usernameResult.innerHTML = checkUsername(username.value);
});

// multiple elements event
const btns = document.getElementsByClassName("btn");
for(let btn of btns) {
    btn.addEventListener("click", function(){
        alert("You clicked me");
    });
};
