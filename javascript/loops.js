//Task 1
let brojac = 0;
while (brojac < 10) {
    console.log("Jas ke ucam JS");
    brojac++;
}

//Task 2
//let counter = 0;
//let res = [];
//while(counter<4){
//    let value = prompt(`Vnesi broj na pozicija ${counter}`);
//    let valueAsNumber = parseInt(value);
//    res.push(valueAsNumber);
//    counter++;
//}
//console.log(`Print num array: ${res}`);



//testArr.length
//let maxValue = testArr[0];
//if(testArr[0] > testArr[1])
//if(testArr[1] > testArr[2])
//if(testArr[3] > testArr[4])
//if(testArr[5] > testArr[6])

let testArr = [180, 50, 40, 1000, 8, 200, 65, 6];
let br = 0;
let maxValue = testArr[0];
while (br < testArr.length) {
    if (maxValue < testArr[br]) {  //testArr[0], testArr[1], testArr[2],
        maxValue = testArr[br];
    }
    br++;
}
console.log(`Max value is ${maxValue}`);


//Task 3
const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
for (let i = 0; i < days.length; i++) {
    console.log(`Days of week: ${days[i]}`);
}

for (let i = days.length - 1; i >= 0; i--) {
    console.log(`Days of week: ${days[i]}`);
}

//Task 4
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] % 2 === 0) {
        console.log(`${numbers[i]} e paren broj`)
    } else if (numbers[i] % 2 != 0) {
        console.log(`${numbers[i]} e neparen broj`)
    } else {
        console.log("Vnesovte gresen broj")
    }
}

for(let i = 0; i <= 10; i++){
    if(i % 2 === 0){
        console.log(`${[i]} e paren broj`)
    }else {
        console.log(`${[i]} e neparen broj`)
    } 
}

//Task 5
for(let i = 0; i <= 10; i++){
    console.log(`${i * 9}`);
}

for(let i = 0; i <= 10; i++){
    let result = i * 9;
    console.log(`${result}`);
}

//Task 6
function assignGrade(){
    for(let i = 60; i < 70; i++){
        console.log(`For ${i} you got a D`);
    }
    for(let i = 70; i < 80; i++){
        console.log(`For ${i} you got a C`);
    }
    for(let i = 80; i < 90; i++){
        console.log(`For ${i} you got a B`);
    }
    for(let i = 90; i <= 100; i++){
        console.log(`For ${i} you got an A`);
    }
}

assignGrade();

//Drug nacin
// Task 4 od if
function assingGrade(score) {
    if (score > 0 && score < 65) {
        console.log('A');
    }
    else if (score >= 65 && score < 70) {
        console.log('B');
    }
    else if (score >= 70 && score < 80) {
        console.log('C');
    }
    else if (score >= 80 && score < 90) {
        console.log('D');
    }
    else if (score >= 90 && score <= 100) {
        console.log('F');
    } else {
        console.log('Enter between 0-100');
    }
}

for (let i = 60; i <= 100; i++){
    assingGrade(i);
}

//Task 7
function charCounter(niza, char){
    let counter = 0;
    for(i = 0; i < niza.length; i++){
        if(niza[i] === char) {
            counter++;
        }
    }
    return counter;
}

let t1 = charCounter(['t', 'e' ,'x', 't'], 't');  
let t2 = charCounter(['t', 'e' ,'x', 't', 't'], 't');
let t3 = charCounter(['t', 'e' ,'x', 't', 't'], 'w');
console.log(`t vo nizata se pojavuva ${t1} pati`);
console.log(`t vo nizata se pojavuva ${t2} pati`);
console.log(`t vo nizata se pojavuva ${t3} pati`);

//Top 8
function biggerThan(inputArray, elementToCompare){
    let resultArray = [];
    for(i = 0; i < inputArray.length; i++){
        if(inputArray[i] > elementToCompare){
            resultArray.push(inputArray[i]);
        }
    }
    return resultArray;
}

let bt1 = biggerThan([4,5,8,1,3],4);
console.log(bt1);
let bt2 = biggerThan([2,3,8,10,15],3);
console.log(bt2);
