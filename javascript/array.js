//Task 1       0        1       2           3       4       5
let colors = ["red", "green" ,"yellow", "orange", "black", "pink"];
console.log(colors[3]);
console.log(colors[4]);

let mixedArray = [1, 2, "text here", true];

//change element - niza[1]="nesto"
console.log(`Print colors before change: ${colors}`);
colors[1] = "limegreen";
console.log(`Print colors: ${colors}`);

//length - niza.length
let colorsLength = colors.length;
console.log(`The array has ${colorsLength} elements`);

//get last element of array - niza[niza.length-1]
let theLastElementOfColors = colors[colors.length - 1];
console.log(`The last element of colors is ${theLastElementOfColors}`);

//add new element na kraj - niza.length
colors[colors.length] = "purple";
colors[colors.length] = "nova boja";
console.log(`Colors with new color added ${colors}`);

//add new element na kraj - niza.push 
colors.push('#ab52cc');
colors.push('#a6666c', '#232323');
console.log(`Colors with new color added ${colors}`);

//add new element na pocetok - niza.unshift 
colors.unshift('white');
colors.unshift('prva boja', 'vtora boja');
console.log(`Colors with new color added in the beggining ${colors}`);

//remove element na kraj - niza.pop()
console.log(`Result before remove : ${colors}`);
let removedLastItem = colors.pop();
console.log(`Removed items: ${removedLastItem}`);
console.log(`Result after remove with pop : ${colors}`);

//remove element na pocetok - niza.shift()
console.log(`Result after remove with shift : ${colors}`);
let removedFirstItem = colors.shift();
console.log(`Removed item ${removedFirstItem}`);
console.log(`Result after remove with shift : ${colors}`);

//add or remove element na koe mesto koj index sakam - niza.splice(index, brisi, dodadi) - koj index, kolku elementi da brise, koj i kolku elementi da se dodade
console.log(`Result before using splice : ${colors}`);
colors.splice(4, 0, "NOVA BOJA");  //add NOVA BOJA at index 4
console.log(`Result after using splice : ${colors}`);
colors.splice(4, 2, "NOVA BOJA"); //add NOVA BOJA at index 4 and delete 2 items after NOVA BOJA
console.log(`Result after using splice : ${colors}`);



//Task 2
let fruits = ["apple", "banana", "lemon"];

let secondElement = fruits[1];
console.log(secondElement);
console.log(fruits[1]);

let lastElement = fruits[fruits.length-1];
console.log(lastElement);

fruits.push("posleden element");
console.log(fruits);

let numberOfElements = fruits.length;
console.log(numberOfElements);
console.log(fruits.length);

fruits.splice(2,0,"vtor element");
console.log(fruits);

let bananaItem = fruits[1];
fruits.splice(1,1,"kiwi");
fruits.push(bananaItem);
console.log(fruits);