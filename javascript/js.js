// Task 1
let age = 22;
const ddv = 16;
let broj = "6";
let daliSumBuden = true;

let students = ["boban", "john", "doe"];
let months = [1, 2, 3, 4];
let objectLiteral = {
    name: "John",
    lastName: "Doe"
};

// Task 2
const feet = 0.3048;
let meters = 56 ;
let metersInfeet = feet * meters;
console.log(metersInfeet);

// Task 3
let a = 5;
let b = 4;
let L = a*b;
console.log(L);

// Task 4
let r = 4;
const  pi = 3.141592653589793;
let area = pi*r*r;
console.log(area);

// Task 5
let day = 28;
let month = 1;
 let year = 2021;
 let className = "JavaScript";
 let res1 = "Today is " + day + " " + month + " "  + year + " " + "and we're learning " + className + " basics!" ;
 console.log(res1);
 let res2 = `Today is ${day} ${month} ${year} and we're learning ${className} basics!`;
 console.log(res2);
