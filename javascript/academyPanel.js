let myApp = document.getElementById("app");
let titleDiv = document.getElementById("title");
let contentDiv = document.getElementById("content");

let students = ["Bob Bobsky", "Jill Cool", "John Doe", "Jane Sky"];
let subjects = ["Math", "English", "Science", "Sport","JavaScript"];
let grades = ["A", "B", "A", "C","A"];

// Math: A
// English: B
function printGrades(subjects, grades, element){
    //element.innerHTML = "";
    let htmlAsString = "";
    htmlAsString += "<ul class='grades-list'>";
    for(let i=0; i<subjects.length; i++){
        htmlAsString += `<li> ${subjects[i]}: ${grades[i]} </li>`;
    }
    htmlAsString += "</ul>";
    console.log(htmlAsString);
    element.innerHTML += htmlAsString;
}

//printGrades(subjects, grades, contentDiv);

function printStudents(students, element){
    //element.innerHTML = "";
    let html = "";
    html += "<ol>";
    for(let student of students){
        html += `<li> ${student}</li>`;
    }
    html += "</ol>";
    element.innerHTML += html;
}

// printStudents(students, contentDiv);
//personType: student or teacher

function academyPanel(personType, name){
    if(personType === "student"){
        titleDiv.innerHTML += `<h1> Hello student, ${name} </h1>`;
        contentDiv.innerHTML += '<h3> Your grades: </h3>';
        contentDiv.innerHTML += '<p> Notes: Today we will learn about JS innerHTML </p>';
        printGrades(subjects, grades, contentDiv);
    }else if(personType === "teacher"){
        titleDiv.innerHTML += `<h1> Hello teacher, ${name} </h1>`;
        contentDiv.innerHTML += `<h3> The list of students </h3>`;
        printStudents(students, contentDiv);
    }else{
        titleDiv.innerHTML = "<h3 style='color: red'> Unexpected input ${name} </h3>";
    }
}

//academyPanel("student", "John");
//academyPanel("teacher", "Doe");
//academyPanel("nesto", "Doe");

let personType = prompt("Are you a student or a teacher?");
let name = prompt("What is your name?");
academyPanel(personType, name);