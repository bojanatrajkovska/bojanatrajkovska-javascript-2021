//Task 1
function doNothing (){
    console.log("ne pravam nisto,samo pecatam vo console");
}

doNothing();

//Task 2
function calcXandY(x,y){
    let result = x*y;
    console.log(`Result : ${x} * ${y} = ${result}`);
    return result;
}

calcXandY(2,3);
calcXandY(50,165);
calcXandY(100,3000);

let result6and5 = calcXandY(6,5);
console.log(`Result from variable ${result6and5}`);

//Task 3
function sumOfTwoNumbers(num1, num2){
    let sum = num1+num2;
    console.log(`The sum of ${num1} and ${num2} is ${sum}`);
    return sum;
}

sumOfTwoNumbers(2,3);
sumOfTwoNumbers(2,10);

let sum3and10 = sumOfTwoNumbers(3,10);
console.log(`The sum of ${3} + ${10} = ${sum3and10}`);

//Task 4
function celsiusToFahrenheit(celsius){
    let fahrenheit = (celsius*9)/5+32;
    console.log(`${celsius} celsius is ${fahrenheit} fahrenheit`);
    return fahrenheit;
}

celsiusToFahrenheit(20);
let c25f = celsiusToFahrenheit(25);

//Task 5
function FahrenheitToCelsius(fahrenheit){
    let celsius = ((fahrenheit-32)*5)/9;
    console.log(`${fahrenheit} fahrenheit is ${celsius} celsius`);
    return celsius;
}

let f77c = FahrenheitToCelsius(77);
console.log(`Is 25 Celsius === 77 Fahrenheit ${ c25f === f77c }`);  //77!=25

//Task 6
function tellFortune(numberOfChildren, partnersName, location, jobTitle){
    let result = `You will be a ${jobTitle} in ${location}, and married to ${partnersName} with ${numberOfChildren} kids.`;
    console.log(result);
    return result;
}

tellFortune (4, "Anabela", "Skopje", "Shofer") ;
tellFortune (3, "Anabela", "Veles", "Pejac") ;
tellFortune (2, "Anabela", "Ohrid", "Programer") ;