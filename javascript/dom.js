const myTitle = document.getElementById("myTitle");
console.log(myTitle);

const myParagraph = document.getElementsByClassName("myParagraph");
console.log(myParagraph);
console.log(myParagraph[0]);

const paragraphs = document.getElementsByTagName("p");
console.log(paragraphs);
const divs = document.getElementsByTagName("div");
console.log(divs);

const myTitleQs = document.querySelector("#myTitle");
console.log(myTitleQs);
const myParagraphQs = document.querySelector(".myParagraph");
console.log(myParagraphQs);
const myParagraphsQs = document.querySelectorAll(".myParagraph");
console.log(myParagraphsQs);

//siblings next or prev
let firstParagraph = document.getElementsByClassName("myParagraph")[0];
console.log(firstParagraph);
let nsibling = firstParagraph.nextElementSibling;
console.log(nsibling);
let psibling = firstParagraph.previousElementSibling;
console.log(psibling);

//parent
let parentOfFirstParagraph = firstParagraph.parentElement;
console.log(parentOfFirstParagraph);


//children
let mainDiv = document.querySelector("#main");
let childrenOfMainDiv = mainDiv.children;
console.log(childrenOfMainDiv);
console.log(mainDiv.firstElementChild);
console.log(mainDiv.lastElementChild);

//getting content
let mainDivTextContent = mainDiv.textContent; //gets
//mainDiv.textContent = "nov content od Js"; //changes
console.log(mainDivTextContent);

let mainDivInnerText = mainDiv.innerText; //gets
//mainDivInnerText = "promena od JS"; //changes
console.log(mainDivInnerText);

//change element
myTitle.innerText; //get text from the element
myTitle.innerText = ""; // deletes the content
myTitle.innerText = "Nov text za naslov"; 
myTitle.innerText += " Sakam da dopisam text";
let currentTitle = myTitle.innerText;
myTitle.innerText = `nov text pred stariot ${currentTitle}`;

//change or add code
let htmlOfMainDiv = mainDiv.innerHTML;
console.log(htmlOfMainDiv);
mainDiv.innerHTML += `<p class="nova-klasa"> dodavam nov paragraph od JS </p>`;
mainDiv.innerHTML = "";

//Exercise 2
let articles = ["pc", "mouse", "monitor", "camera", "mouse pad"];
let prices = [25000, 550, 16000, 1000, 400];
let areaArticlesInStock = [true, false, true, true, false];
const resultDiv = document.getElementById("result");

function printArticles(articles, prices, isArrInStock, result){
    let htmlAsText = "";
    htmlAsText = "<ol class='article-list'>";
    for(let i=0; i<articles.length; i++){
    htmlAsText += `<li> 
                    <div class="name">Article: ${articles[i]} </div>
                    <div class="price">Price: ${prices[i]} </div>
                    <div class="is-in-stock ${isArrInStock[i] ? 'available' : ''}"> Is in stock: ${isArrInStock[i] ? "Yes" : "No"}</div>
                    </li>`;
    }
    htmlAsText += "</ol>";
    result.innerHTML += htmlAsText;
}

    printArticles(articles, prices, areaArticlesInStock, resultDiv);