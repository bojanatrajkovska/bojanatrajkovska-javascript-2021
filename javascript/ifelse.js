// Task 1
let score = 60;
let result = score > 90;
console.log(typeof result);

if (score > 90) {
    console.log("Congrats!!!")
} else {
    console.log("Sorry see you next year")
}


if (score > 50) {
    console.log('Score > 50');
} else if (score < 50) {
    console.log('Score < 50');
} else if (score == 50) {
    console.log('Score = 50');
} else {
    console.log('unexpected input');
}

// Task 2
let keshInput = prompt('Vnesi suma');
console.log(typeof keshInput);//'2000'  od prompt sekogas string se dobiva - ako e broj - ono go gleda ko string
console.log(`Vie vnesovte ${keshInput}`);

let keshAsNumber = parseInt(keshInput); // parseInt za da go smenis string vo int number

const fridayCash = keshAsNumber;
console.log(typeof fridayCash);

if (fridayCash >= 0) {
    if (fridayCash >= 50) {
        console.log('You can go for a dinner and movie');
    } else if (fridayCash >= 35) {
        console.log('You can go for a meal');
    } else if (fridayCash >= 12) {
        console.log('You can see a movie');
    } else if (fridayCash < 12) {
        console.log('You\'ll watch TV');
    } else {
        console.log('Unexpected input');
    }
} else {
    console.log('Vnesi kesh pogolem od 0');
}

// Task 3
function greaterNum(num1, num2) {
    if (num1 > num2) {
        console.log(`${num1} is greater than ${num2}`);
    } else if (num1 < num2) {
        console.log(`${num2} is greater than ${num1}`);
    } else if (num1 === num2) {
        console.log(`${num1} is equal with ${num2}`);
    } else {
        console.log('Something happened');
    }
}

greaterNum(10, 4);
greaterNum(5, 15);
greaterNum(5, 5);
greaterNum(4);
greaterNum('text', 4);

// Task 4
function assingGrade(score) {
    if (score >= 90 && score <= 100) {
        console.log('A');
    }
    else if (score >= 80 && score <= 89) {
        console.log('B');
    }
    else if (score >= 70 && score <= 79) {
        console.log('C');
    }
    else if (score >= 60 && score <= 69) {
        console.log('D');
    }
    else if (score < 60 && score >= 0) {
        console.log('F');
    } else {
        console.log('Enter between 0-100');
    }
}

assingGrade(1500);
assingGrade(95);
assingGrade(86);
assingGrade(70);
assingGrade(69);
assingGrade(50);
assingGrade(-50);
 
//Druga zadaca od for
for (let i = 60; i <= 100; i++){
    assingGrade(i);
}

// Task 5
function helloWorld(language) {
    if (language == 'es') {
        console.log('Holla');
    }
    else if (language == 'de') {
        console.log('Halo');
    }
    else if (language == 'en') {
        console.log('Hello');
    }
    else if (language == 'mk') {
        console.log('Zdravo');
    }
    else {
        console.log('Unsupported language');
    }
}

helloWorld('es');
helloWorld('de');
helloWorld('en');
helloWorld('mk');
helloWorld('usa');
helloWorld('');
helloWorld('4');

// Task 6
function helloWorldWithSwitch(language) {
    switch (language) {
        case 'es':
            console.log('Holla');
            break;
        case 'de':
            console.log('Halo');
            break;
        case 'en':
        case 'usa':
            console.log('Hello');
            break;
        case 'mk':
            console.log('Zdravo');
            break;
        default:
            console.log('Unsupported language');
            break;
    }
}

helloWorldWithSwitch('mk');
helloWorldWithSwitch('en');
helloWorldWithSwitch('usa');

// Task 7
function chineseZodiac(year) {
    let result = (year-4)%12;
    switch (result) {
        case 0:
            console.log('Rat');
            break;
        case 1:
            console.log('Ox');
            break;
        case 2:
            console.log('Tiger');
            break;
        case 3:
            console.log('Rabbit');
            break;
        case 4:
            console.log('Dragon');
            break;
        case 5:
            console.log('Snake');
            break;
        case 6:
            console.log('Horse');
            break;
        case 7:
            console.log('Goat');
            break;
        case 8:
            console.log('Monkey');
            break;
        case 9:
            console.log('Rooster');
            break;
        case 10:
            console.log('Dog');
            break;
        case 11:
            console.log('Pig');
            break;
        default:
            console.log('Wrong input for year');
            break;
    }
}

chineseZodiac(1998);
chineseZodiac(1990);
chineseZodiac(1995);
chineseZodiac(1986);
chineseZodiac('text');
